package com.achthoven.weatherforecast.model;

import java.util.HashMap;
import java.util.Map;

public class WeatherQueryResponse {

    private Query query;
    private final Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The query
     */
    public Query getQuery() {
        return query;
    }

    /**
     * @param query The query
     */
    public void setQuery(Query query) {
        this.query = query;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
