package com.achthoven.weatherforecast.dagger;

import com.achthoven.weatherforecast.ForecastApplication;
import com.achthoven.weatherforecast.gui.DaggerForecastActivityTest_TestApplicationComponent;

public class MockForecastApplication extends ForecastApplication {
    @Override
    protected ApplicationComponent createComponent() {
        return DaggerForecastActivityTest_TestApplicationComponent.builder()
                .build();
    }
}
