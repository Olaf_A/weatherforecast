package com.achthoven.weatherforecast;

import com.achthoven.weatherforecast.model.WeatherQueryResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface YahooWeatherAPI {

    //?q=select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="{city}")

    @GET("yql")
    Call<WeatherQueryResponse> getForecast(@Query("q") String query, @Query("format") String format, @Query("env") String environment);
}
