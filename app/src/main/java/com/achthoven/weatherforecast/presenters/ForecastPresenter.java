package com.achthoven.weatherforecast.presenters;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.achthoven.weatherforecast.ForecastFetcher;
import com.achthoven.weatherforecast.contracts.ForecastContract;
import com.achthoven.weatherforecast.model.WeatherQueryResponse;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForecastPresenter implements ForecastContract.UserActions {

    @Nullable
    private ForecastContract.ViewActions mView;

    @NonNull
    private final ForecastFetcher mFetcher;

    @Nullable
    private WeatherQueryResponse mQueryResponse;

    @Inject
    public ForecastPresenter(@NonNull final ForecastFetcher forecastFetcher) {
        mFetcher = forecastFetcher;
    }

    @Override
    public void bind(final ForecastContract.ViewActions view) {
        mView = view;

        loadForecast();
    }

    @Override
    public void loadForecast() {
        assert mView != null;

        mView.showLoading();
        mFetcher.getData(new Callback<WeatherQueryResponse>() {
            @Override
            public void onResponse(Call<WeatherQueryResponse> call, Response<WeatherQueryResponse> response) {
                mQueryResponse = response.body();
                mView.updateForecast(mQueryResponse);
            }

            @Override
            public void onFailure(Call<WeatherQueryResponse> call, Throwable t) {
                mQueryResponse = null;
                mView.updateFailed();
            }
        });
    }
}
