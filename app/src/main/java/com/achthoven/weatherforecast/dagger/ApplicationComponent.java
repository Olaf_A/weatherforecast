package com.achthoven.weatherforecast.dagger;

import com.achthoven.weatherforecast.ForecastFetcher;
import com.achthoven.weatherforecast.gui.ForecastActivity;

public interface ApplicationComponent {
    ForecastFetcher forecastFetcher();

    void inject(ForecastActivity mainActivity);
}

