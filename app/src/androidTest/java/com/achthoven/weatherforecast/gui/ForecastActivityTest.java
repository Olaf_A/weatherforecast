package com.achthoven.weatherforecast.gui;

import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.rule.UiThreadTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.achthoven.weatherforecast.ForecastApplication;
import com.achthoven.weatherforecast.ForecastFetcher;
import com.achthoven.weatherforecast.R;
import com.achthoven.weatherforecast.dagger.ApplicationComponent;
import com.achthoven.weatherforecast.dagger.MockForecastFetcherModule;
import com.achthoven.weatherforecast.model.Channel;
import com.achthoven.weatherforecast.model.Forecast;
import com.achthoven.weatherforecast.model.Item;
import com.achthoven.weatherforecast.model.Query;
import com.achthoven.weatherforecast.model.Results;
import com.achthoven.weatherforecast.model.Units;
import com.achthoven.weatherforecast.model.WeatherQueryResponse;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.swipeDown;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.achthoven.weatherforecast.EspressoUtils.atPosition;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.hamcrest.core.IsNot.not;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(AndroidJUnit4.class)
public class ForecastActivityTest {
    private static final String FORECAST_TITLE = "Title";

    @Rule
    public final ActivityTestRule<ForecastActivity> mActivityRule = new ActivityTestRule(ForecastActivity.class, true, false);
    @Rule
    public UiThreadTestRule mUIThreadRule = new UiThreadTestRule();

    @Inject
    ForecastFetcher mMockFetcher;

    @Singleton
    @Component(modules = MockForecastFetcherModule.class)
    public interface TestApplicationComponent extends ApplicationComponent {
        void inject(ForecastActivityTest forecastActivityTest);
    }

    private final WeatherQueryResponse mTestResponseWithItems = setupWeatherQueryResponse(true);
    private final WeatherQueryResponse mTestResponseWithoutItems = setupWeatherQueryResponse(false);

    private static WeatherQueryResponse setupWeatherQueryResponse(final boolean withItems) {
        WeatherQueryResponse response = new WeatherQueryResponse();
        Query query = new Query();
        Results results = new Results();
        Channel channel = new Channel();
        Item item = new Item();

        List<Forecast> forecasts = new ArrayList<>();

        if (withItems) {
            Forecast forecast1 = new Forecast();
            forecast1.setDay("Monday");
            forecast1.setDate("01-01-2016");
            forecast1.setLow("22");
            forecast1.setHigh("31");
            forecasts.add(forecast1);

            Forecast forecast2 = new Forecast();
            forecast2.setDay("Tuesday");
            forecast2.setDate("01-02-2016");
            forecast2.setLow("10");
            forecast2.setHigh("41");
            forecasts.add(forecast2);
        }

        item.setForecast(forecasts);
        item.setTitle(FORECAST_TITLE);
        channel.setItem(item);

        Units units = new Units();
        units.setTemperature("F");
        channel.setUnits(units);

        results.setChannel(channel);
        query.setResults(results);
        response.setQuery(query);

        return response;
    }

    private void setupSuccessFetcher(final boolean withItems) {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<WeatherQueryResponse> callback = (Callback<WeatherQueryResponse>) invocation.getArguments()[0];
                if (callback != null) {
                    Response<WeatherQueryResponse> response = Response.success(withItems ? mTestResponseWithItems : mTestResponseWithoutItems);
                    callback.onResponse(null, response);
                }
                return null;
            }
        }).when(mMockFetcher).getData(Matchers.<Callback<WeatherQueryResponse>>any());
    }

    private void setupFailureFetcher() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<WeatherQueryResponse> callback = (Callback<WeatherQueryResponse>) invocation.getArguments()[0];
                if (callback != null) {
                    callback.onFailure(null, new Throwable("Mocked error"));
                }
                return null;
            }
        }).when(mMockFetcher).getData(Matchers.<Callback<WeatherQueryResponse>>any());
    }

    private void setupEndlessFetcher() {
        doNothing().when(mMockFetcher).getData(Matchers.<Callback<WeatherQueryResponse>>any());
    }

    private void launchActivity() {
        mActivityRule.launchActivity(new Intent());
    }

    @Before
    public void setUp() {
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        ForecastApplication app = (ForecastApplication) instrumentation.getTargetContext().getApplicationContext();
        TestApplicationComponent component = (TestApplicationComponent) app.component();
        component.inject(this);
    }

    @Test
    public void titleIsComposedFromData() {
        setupSuccessFetcher(true);
        launchActivity();

        onView(withId(R.id.tv_forecast_title)).check(matches(withText(FORECAST_TITLE)));
    }

    @Test
    public void forecastContainsTwoTemperatures() {
        setupSuccessFetcher(true);
        launchActivity();

        onView(withId(R.id.rv_forecast_list))
                .check(matches(atPosition(0, hasDescendant(allOf(withId(R.id.tv_card_temperature), withText(startsWith("22 - 31")))))));
        onView(withId(R.id.rv_forecast_list))
                .check(matches(atPosition(1, hasDescendant(allOf(withId(R.id.tv_card_temperature), withText(startsWith("10 - 41")))))));
    }

    @Test
    public void swipeDownCausesRefresh() {
        setupSuccessFetcher(true);
        launchActivity();

        onView(withId(R.id.rv_forecast_list)).perform(swipeDown());

        // getData should have been called twice (once when creating the view, once on the swipe action)
        verify(mMockFetcher, times(2)).getData(Matchers.<Callback<WeatherQueryResponse>>any());
    }

    @Test
    public void updateErrorShowsToast() {
        setupFailureFetcher();
        launchActivity();

        onView(withText(R.string.update_failed)).inRoot(withDecorView(not(mActivityRule.getActivity().getWindow().getDecorView()))).check(matches(isDisplayed()));
    }

    @Test
    public void emptyListShowsMessage() {
        setupSuccessFetcher(false);
        launchActivity();

        onView(withId(R.id.tv_empty_list)).check(matches(isCompletelyDisplayed()));
    }

    @Test
    public void progressShowsWhileLoading() {
        setupEndlessFetcher();
        launchActivity();

        onView(withId(R.id.pb_loading)).check(matches(isCompletelyDisplayed()));
    }
}