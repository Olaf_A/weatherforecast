package com.achthoven.weatherforecast.model;


import java.util.HashMap;
import java.util.Map;

public class Astronomy {

    private String sunrise;
    private String sunset;
    private final Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The sunrise
     */
    public String getSunrise() {
        return sunrise;
    }

    /**
     * @param sunrise The sunrise
     */
    public void setSunrise(String sunrise) {
        this.sunrise = sunrise;
    }

    /**
     * @return The sunset
     */
    public String getSunset() {
        return sunset;
    }

    /**
     * @param sunset The sunset
     */
    public void setSunset(String sunset) {
        this.sunset = sunset;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
