package com.achthoven.weatherforecast.dagger;

import com.achthoven.weatherforecast.ForecastFetcher;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ForecastFetcherModule {
    @Provides
    @Singleton
    ForecastFetcher provideForecastFetcher() {
        return new ForecastFetcher();
    }

}
