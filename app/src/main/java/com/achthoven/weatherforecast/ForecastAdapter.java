package com.achthoven.weatherforecast;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.achthoven.weatherforecast.model.Forecast;

import java.util.List;

public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ViewHolder> {
    private final Context mContext;
    private final String mTemperatureUnits;
    private final List<Forecast> mItems;

    public class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView mTitle;
        private final TextView mTemperature;
        private final TextView mForecast;

        public ViewHolder(final View view) {
            super(view);
            mTitle = (TextView) view.findViewById(R.id.tv_card_title);
            mTemperature = (TextView) view.findViewById(R.id.tv_card_temperature);
            mForecast = (TextView) view.findViewById(R.id.tv_card_forecast);
        }
    }

    public ForecastAdapter(final Context context, final List<Forecast> items, final String temperatureUnits) {
        mContext = context;
        mItems = items;
        mTemperatureUnits = temperatureUnits;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.forecast_card, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Forecast forecast = mItems.get(position);

        final String titleString = String.format(mContext.getString(R.string.forecast_date_format), forecast.getDay(), forecast.getDate());
        final String temperatureString = String.format(mContext.getString(R.string.forecast_temperature_format), forecast.getLow(), forecast.getHigh(), mTemperatureUnits);

        holder.mTitle.setText(titleString);
        holder.mTemperature.setText(temperatureString);
        holder.mForecast.setText(forecast.getText());
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

}
