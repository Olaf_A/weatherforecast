package com.achthoven.weatherforecast;

import android.app.Application;

import com.achthoven.weatherforecast.dagger.ApplicationComponent;
import com.achthoven.weatherforecast.dagger.ForecastFetcherModule;

import javax.inject.Singleton;

import dagger.Component;

public class ForecastApplication extends Application {
    @Singleton
    @Component(modules = ForecastFetcherModule.class)
    public interface ForecastApplicationComponent extends ApplicationComponent {
    }

    private final ApplicationComponent component = createComponent();

    protected ApplicationComponent createComponent() {
        return DaggerForecastApplication_ForecastApplicationComponent.builder()
                .build();
    }

    public ApplicationComponent component() {
        return component;
    }
}
