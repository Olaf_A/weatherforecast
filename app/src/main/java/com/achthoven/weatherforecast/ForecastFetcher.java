package com.achthoven.weatherforecast;

import com.achthoven.weatherforecast.model.WeatherQueryResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ForecastFetcher {
    // Trailing slash is needed
    public static final String BASE_URL = "https://query.yahooapis.com/v1/public/";

    public void getData(final Callback<WeatherQueryResponse> callback) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        YahooWeatherAPI yahooApi = retrofit.create(YahooWeatherAPI.class);


        final String query = "select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\"amsterdam\")";
        final String format = "json";
        final String env = "store://datatables.org/alltableswithkeys";

        final Call<WeatherQueryResponse> call = yahooApi.getForecast(query, format, env);
        call.enqueue(callback);
    }
}
