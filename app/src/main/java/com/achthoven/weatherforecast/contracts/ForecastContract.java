package com.achthoven.weatherforecast.contracts;

import com.achthoven.weatherforecast.model.WeatherQueryResponse;

public interface ForecastContract {

    interface ViewActions {

        void showLoading();
        void updateForecast(WeatherQueryResponse mQueryResponse);
        void updateFailed();
    }

    interface UserActions {

        void bind(ViewActions viewActions);

        void loadForecast();
    }

}
