package com.achthoven.weatherforecast.dagger;

import com.achthoven.weatherforecast.ForecastFetcher;
import com.achthoven.weatherforecast.presenters.ForecastPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ForecastPresenterModule {
    @Provides
    @Singleton
    ForecastPresenter provideForecastPresenter(ForecastFetcher fetcher) {
        return new ForecastPresenter(fetcher);
    }
}
