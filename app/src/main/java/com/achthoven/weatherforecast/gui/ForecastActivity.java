package com.achthoven.weatherforecast.gui;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.achthoven.weatherforecast.ForecastAdapter;
import com.achthoven.weatherforecast.ForecastApplication;
import com.achthoven.weatherforecast.R;
import com.achthoven.weatherforecast.contracts.ForecastContract;
import com.achthoven.weatherforecast.model.Channel;
import com.achthoven.weatherforecast.model.Forecast;
import com.achthoven.weatherforecast.model.WeatherQueryResponse;
import com.achthoven.weatherforecast.presenters.ForecastPresenter;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

public class ForecastActivity extends AppCompatActivity implements ForecastContract.ViewActions {

    @Inject
    ForecastPresenter mPresenter;

    private List<Forecast> mForecastItems = Collections.EMPTY_LIST;
    private ForecastAdapter mForecastAdapter;

    private TextView mTitle;
    private RecyclerView mForecastList;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ProgressBar mLoadingProgress;
    private TextView mEmptyListText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.rl_swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mPresenter != null) {
                    mPresenter.loadForecast();
                }
            }
        });

        mTitle = (TextView) findViewById(R.id.tv_forecast_title);
        mLoadingProgress = (ProgressBar) findViewById(R.id.pb_loading);
        mForecastList = (RecyclerView) findViewById(R.id.rv_forecast_list);
        mEmptyListText = (TextView) findViewById(R.id.tv_empty_list);

        mForecastAdapter = new ForecastAdapter(this, mForecastItems, "");

        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mForecastList.setLayoutManager(mLayoutManager);
        mForecastList.setItemAnimator(new DefaultItemAnimator());
        mForecastList.addItemDecoration(new VerticalSpaceItemDecoration(this, 10));
        mForecastList.setAdapter(mForecastAdapter);

        ((ForecastApplication) getApplication()).component().inject(this);
        mPresenter.bind(this);
    }

    @Override
    public void showLoading() {
        mForecastList.setVisibility(View.GONE);
        mEmptyListText.setVisibility(View.GONE);
        mLoadingProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void updateForecast(final WeatherQueryResponse mQueryResponse) {
        final Channel forecastChannel = mQueryResponse.getQuery().getResults().getChannel();

        mTitle.setText(forecastChannel.getItem().getTitle());

        mForecastItems = forecastChannel.getItem().getForecast();
        mForecastAdapter = new ForecastAdapter(this, mForecastItems, forecastChannel.getUnits().getTemperature());
        mForecastList.setAdapter(mForecastAdapter);

        mLoadingProgress.setVisibility(View.GONE);
        if (mForecastItems.isEmpty()) {
            mForecastList.setVisibility(View.GONE);
            mEmptyListText.setVisibility(View.VISIBLE);
        } else {
            mForecastList.setVisibility(View.VISIBLE);
            mEmptyListText.setVisibility(View.GONE);
        }

        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void updateFailed() {
        Toast.makeText(this, R.string.update_failed, Toast.LENGTH_LONG).show();
    }
}
