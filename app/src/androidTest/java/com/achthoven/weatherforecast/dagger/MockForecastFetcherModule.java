package com.achthoven.weatherforecast.dagger;

import com.achthoven.weatherforecast.ForecastFetcher;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static org.mockito.Mockito.mock;

@Module
public class MockForecastFetcherModule {
    @Provides
    @Singleton
    ForecastFetcher provideForecastFetcher() {
        return mock(ForecastFetcher.class);
    }

}
