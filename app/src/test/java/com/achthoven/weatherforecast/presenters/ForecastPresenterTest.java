package com.achthoven.weatherforecast.presenters;

import com.achthoven.weatherforecast.ForecastFetcher;
import com.achthoven.weatherforecast.contracts.ForecastContract;
import com.achthoven.weatherforecast.model.WeatherQueryResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import retrofit2.Callback;

import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;

public class ForecastPresenterTest {

    private ForecastContract.ViewActions mMockView;
    private ForecastFetcher mMockForecastFetcher;
    private ForecastPresenter mPresenter;

    @Before
    public void setup() {
        mMockView = mock(ForecastContract.ViewActions.class);
        mMockForecastFetcher = mock(ForecastFetcher.class);
        mPresenter= new ForecastPresenter(mMockForecastFetcher);
    }

    @Test
    public void presenterFetchesDataOnCreate() {
        mPresenter.bind(mMockView);

        verify(mMockForecastFetcher).getData(Matchers.<Callback<WeatherQueryResponse>>any());
    }

    @Test
    public void presenterFetchesDataOnDemand() {
        mPresenter.bind(mMockView);

        // Reset initial interactions
        reset(mMockForecastFetcher);

        mPresenter.loadForecast();
        verify(mMockForecastFetcher).getData(Matchers.<Callback<WeatherQueryResponse>>any());
    }

    @Test
    public void presenterNotfiesOfLoading() {
        mPresenter.bind(mMockView);

        verify(mMockView).showLoading();
    }

    @Test
    public void presenterNotfiesOfFailure() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback<WeatherQueryResponse> callback = (Callback<WeatherQueryResponse>) invocation.getArguments()[0];
                if (callback != null) {
                    callback.onFailure(null, new Throwable("Mocked error"));
                }
                return null;
            }
        }).when(mMockForecastFetcher).getData(Matchers.<Callback<WeatherQueryResponse>>any());

        mPresenter.bind(mMockView);

        verify(mMockView).updateFailed();
    }

}