package com.achthoven.weatherforecast.gui;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

// Item decoration that adds some space below the items
class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {

    private final int mVerticalSpaceHeightDp;

    public VerticalSpaceItemDecoration(final Context context, int verticalSpaceHeightPixels) {
        // Get the screen's density scale
        final float scale = context.getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        mVerticalSpaceHeightDp = (int) (verticalSpaceHeightPixels * scale + 0.5f);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        outRect.bottom = mVerticalSpaceHeightDp;
    }
}
